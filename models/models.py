# -*- coding: utf-8 -*-

from odoo import models, fields, api

class res_company(models.Model):

_inherit=‘res.company’

logo_report= fields.Binary(“Header Image”,

help=“This field holds the image used for the logo on the prints, limited to 1024x1024px”)

class account_invoive_extension(models.Model):

_inherit=‘account.invoice’

# class finnish_invoice_receipt(models.Model):
#     _name = 'finnish_invoice_receipt.finnish_invoice_receipt'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100